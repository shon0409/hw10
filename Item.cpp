#include "Item.h"

Item::Item()
{
}

Item::Item(string name, string serialName, double unitPrice)
{
	_name = name;
	_serialNumber = serialName;
	_unitPrice = unitPrice;
}

Item::~Item()
{
}

string Item::GetName()const
{
	return _name;
}

void Item::SetName(string name)
{
	_name = name;
}

string Item::GetSerialNumber()const
{
	return _serialNumber;
}

void Item::SetSerialNumber(string serialNumber)
{
	_serialNumber = serialNumber;
}

int Item::GetCount()const
{
	return _count;
}

void Item::SetCount(int count)
{
	_count = count;
}

double Item::GetUnitPrice()const
{
	return _unitPrice;
}

void Item::SetUnitPrice(double unitPrice)
{
	_unitPrice = unitPrice;
}

double Item::totalPrice()const
{
	return _count * _unitPrice;
}

bool Item::operator<(const Item & other)const
{
	return _serialNumber < other._serialNumber;
}

bool Item::operator>(const Item & other)const
{
	return _serialNumber > other._serialNumber;
}

bool Item::operator==(const Item & other)const
{
	return _serialNumber == other._serialNumber;
}

