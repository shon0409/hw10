#pragma once

#include <iostream>
using namespace std;

class Item {

public:
	Item();
	Item(string name, string serialName, double unitPrice);
	~Item();

	string GetName()const;
	void SetName(string);
	string GetSerialNumber()const;
	void SetSerialNumber(string);
	int GetCount()const;
	void SetCount(int);
	double GetUnitPrice()const;
	void SetUnitPrice(double);


	double totalPrice()const;
	bool operator<(const Item& other)const;
	bool operator>(const Item& other)const;
	bool operator==(const Item& other)const;
private:
	string _name, _serialNumber;
	int _count = 1;
	double _unitPrice;
};
