#include "Customer.h"

double Customer::totalSum()
{
	double sum = 0;
	for (set<Item>::iterator it = _items.begin(); it != _items.end(); ++it)
	{
		sum += it->totalPrice();
	}
	return sum;
}

void Customer::addItem(Item item)
{
	if (_items.find(item) != _items.end())//checking if item exists
	{
		set<Item>::iterator ierItem = _items.find(item);
		const_cast<Item&>(*ierItem).SetCount(ierItem->GetCount() + 1);//adding to the item count
	}
	else
	{
		_items.insert(item);
	}
}

void Customer::removeItem(Item item)
{
	if (item.GetCount() <= 1)//??????
	{
		this->_items.erase(item);
	}
	else
	{
		set<Item>::iterator ierItem = _items.find(item);
		const_cast<Item&>(*ierItem).SetCount(item.GetCount() - 1);
	}
}

Customer::Customer()
{
}

Customer::Customer(const Customer & customer)
{
	this->_name = customer._name;
	this->_items = customer._items;
}

Customer::Customer(string name, set<Item> items)
{
	this->_name = name;
	this->_items = items;
}

Customer::Customer(string name)
{
	_name = name;
	_items = set<Item>();
}

Customer::~Customer()
{
}

string Customer::GetName()
{
	return _name;
}

void Customer::SetName(string name)
{
	_name = name;
}

set<Item> Customer::GetItems()
{
	return _items;
}

void Customer::SetItems(set<Item> items)
{
	_items = items;
}
